import axios from 'axios'
axios.defaults.headers.post['X-Requested-With'] = 'XMLHttpRequest'

export const state = () => ({
    posts:[],
    postInfo:{},

    podcasts:[
        'GOOGLE',
        'SPOTIFY',
        'PLAYERFM',
        'SOUNDCLOUD',
        'APPLE',
        'TUNEIN',
        'YANDEX',
        'VK',
        'YOUTUBE',
    ],

    podcastImages:{
        apple:require('~/assets/img/icons/podcasts/apple.svg'),
        google:require('~/assets/img/icons/podcasts/google.svg'),
        playerfm:require('~/assets/img/icons/podcasts/player.svg'),
        soundcloud:require('~/assets/img/icons/podcasts/soundcloud.svg'),
        spotify:require('~/assets/img/icons/podcasts/spotify.svg'),
        tunein:require('~/assets/img/icons/podcasts/tunein.svg'),
        vk:require('~/assets/img/icons/podcasts/vk.svg'),
        yandex:require('~/assets/img/icons/podcasts/yandex.svg'),
        youtube:require('~/assets/img/icons/podcasts/youtube.svg'),
        listennotes:require('~/assets/img/icons/podcasts/listennotes.svg'),
        mytuner:require('~/assets/img/icons/podcasts/mytuner.svg'),
    }
})

export const mutations = {
    setPostDetail(state,data){
        console.log(data)

        // Обработка ссылок для разных платформ подкастов
        let linksArr = []
        state.podcasts.map((item)=>{
            if(data[item]){
                linksArr.push({
                    link:data[item],
                    img:state.podcastImages[item.toLowerCase()],
                    title:item.toLowerCase(),
                })
            }
        })
        data.LINKS = linksArr

        // Данный участок кода необходимо, чтобы поставить SEO изобрежение для определенного поста
        let selectedPost =  state.posts.filter((item)=>{
            return item.route=='/'+data.CODE
        })[0]
        data.PREVIEW_PICTURE = selectedPost.image

        // Обработка главного элемента, чтобы корректно выводить span элементы
        data.QUOTE = data.QUOTE.replace('&lt;span&gt;', '<span>')
        data.QUOTE = data.QUOTE.replace('&lt;/span&gt;', '</span>')

        // обработка похожих проектов для сайта
        let sprojects = []

        sprojects = data.S_PROJECTS.map((item)=>{
            let obj = {
                route:'/' + item.CODE,
                outsideLink:false,
                image:item.PREVIEW_PICTURE,
                date:item.ACTIVE_FROM.split(' ')[0],
                text:item.NAME,
                id:item.ID
            }

            return obj
        })

        data.S_PROJECTS = sprojects

        state.postInfo = data
    },

    setPostsList(state,data){
        let podcasts = []
        
        // Обработка выпусков по подкасту
        podcasts = data.map((item)=>{
            let obj = {
                route:'/' + item.CODE,
                outsideLink:false,
                number:item.RELEASE.length<2?'0'+item.RELEASE:item.RELEASE,
                sort:item.SORT,
                image:item.PREVIEW_PICTURE,
                date:item.ACTIVE_FROM.split(' ')[0],
                text:item.NAME,
                id:item.ID
            }

            return obj
        })

        state.posts = podcasts
    }
}

export const actions = {
    async fetchPostDetail({state,commit}, {route}){
        // Тут проверяется загружены ли уже подкасты, и если уже загружены, то становится на один дзапрос меньше
        let podcasts = state.posts 
        if(podcasts.length==0){
            let podcastsRes = await axios.post('https://api.blackandwhite.kz/bw/prhub/?lang=ru')
            await commit('setPostsList',podcastsRes.data.index.PODCASTS)
            console.log(podcastsRes.data.index.PODCASTS)
        }

        // Находит ID для необходимого подкаста
        let selectedPost = await state.posts.filter((item)=>{
            return item.route==route
        })[0]

        let res  = await axios.post(`https://api.blackandwhite.kz/bw/prhub/detail.php?id=${selectedPost.id}&lang=ru`)
        await commit('setPostDetail',res.data.project[0])

    }
}

export const getters = {
    
    getPosts:(state)=>{
        return state.posts
    },

    getPostDetail:(state)=>{
        return state.postInfo
    },
    
}