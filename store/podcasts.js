export const state = ()=>({
    podcasts:[],

    podcastImages:{
        apple:require('~/assets/img/icons/podcasts/apple.svg'),
        google:require('~/assets/img/icons/podcasts/google.svg'),
        playerfm:require('~/assets/img/icons/podcasts/player.svg'),
        soundcloud:require('~/assets/img/icons/podcasts/soundcloud.svg'),
        spotify:require('~/assets/img/icons/podcasts/spotify.svg'),
        tunein:require('~/assets/img/icons/podcasts/tunein.svg'),
        vk:require('~/assets/img/icons/podcasts/vk.svg'),
        yandex:require('~/assets/img/icons/podcasts/yandex.svg'),
        youtube:require('~/assets/img/icons/podcasts/youtube.svg'),
        listennotes:require('~/assets/img/icons/podcasts/listennotes.svg'),
        mytuner:require('~/assets/img/icons/podcasts/mytuner.svg'),
    }
})

export const mutations = {
    setPodcastsList(state,data){

        let linkPodcasts = []

        // Обработка платформ для ссылок по подкастам
        linkPodcasts = Object.entries(data).map((item)=>{
            let name = item[0].toLowerCase()

            let obj = {
                img:state.podcastImages[name],
                title:name,
                link:item[1],
            }

            return obj
        })

        state.podcasts = linkPodcasts
        
    }
}

export const getters = {
    getPodcasts:(state)=>{
        return state.podcasts
    }
}