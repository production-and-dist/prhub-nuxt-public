export const state = () => ({
    publications:[]
})

export const mutations = {
    setPublicationsList(state,data){
        let articles = []

        // ОБработка статей для публикации
        articles = data.map((item)=>{
            let obj = {
                route:item.CODE,
                outsideLink:true,
                image:item.PREVIEW_PICTURE,
                date:item.ACTIVE_FROM.split(' ')[0],
                text:item.NAME,
            }

            return obj
        })

        state.publications = articles
    }
}

export const getters = {
    getPublications:(state)=>{
        return state.publications
    }
}