import axios from 'axios'
axios.defaults.headers.post['X-Requested-With'] = 'XMLHttpRequest'

export const state = () => ({
    info:{},
    menuActiveIndex:0,
})

export const mutations = {
    setMainDetail(state,data){
        state.info = data
    },

    setMenuActive(state,{newIndex}){
        state.menuActiveIndex = newIndex
    }
}

export const actions = {
    async fetchMainInfo({commit}){
        let res  = await axios.post('https://api.blackandwhite.kz/bw/prhub/?lang=ru')
        
        await commit('setMainDetail',res.data.index)
        let indexInfo = res.data.index

        await commit('podcasts/setPodcastsList',indexInfo.LINK_PODCASTS)
        await commit('posts/setPostsList',indexInfo.PODCASTS)
        await commit('publications/setPublicationsList',indexInfo.ARTICLES)
    }
}

export const getters = {
    getMainInfo: (state) => state.info,
    getMenuActiveIndex: (state) => state.menuActiveIndex
}