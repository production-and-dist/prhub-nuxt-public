import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  CommonPersonCard: () => import('../../components/common/PersonCard.vue' /* webpackChunkName: "components/common-person-card" */).then(c => wrapFunctional(c.default || c)),
  CommonPodcastCard: () => import('../../components/common/PodcastCard.vue' /* webpackChunkName: "components/common-podcast-card" */).then(c => wrapFunctional(c.default || c)),
  CommonPostCard: () => import('../../components/common/PostCard.vue' /* webpackChunkName: "components/common-post-card" */).then(c => wrapFunctional(c.default || c)),
  LayoutAppFooter: () => import('../../components/layout/AppFooter.vue' /* webpackChunkName: "components/layout-app-footer" */).then(c => wrapFunctional(c.default || c)),
  LayoutAppHeader: () => import('../../components/layout/AppHeader.vue' /* webpackChunkName: "components/layout-app-header" */).then(c => wrapFunctional(c.default || c)),
  LayoutContactsBlock: () => import('../../components/layout/ContactsBlock.vue' /* webpackChunkName: "components/layout-contacts-block" */).then(c => wrapFunctional(c.default || c)),
  LayoutMainMenu: () => import('../../components/layout/MainMenu.vue' /* webpackChunkName: "components/layout-main-menu" */).then(c => wrapFunctional(c.default || c)),
  LayoutProgressBar: () => import('../../components/layout/ProgressBar.vue' /* webpackChunkName: "components/layout-progress-bar" */).then(c => wrapFunctional(c.default || c)),
  UiAppBtn: () => import('../../components/ui/AppBtn.vue' /* webpackChunkName: "components/ui-app-btn" */).then(c => wrapFunctional(c.default || c)),
  UiAppForm: () => import('../../components/ui/AppForm.vue' /* webpackChunkName: "components/ui-app-form" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
