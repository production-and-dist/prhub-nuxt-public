# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<CommonPersonCard>` | `<common-person-card>` (components/common/PersonCard.vue)
- `<CommonPodcastCard>` | `<common-podcast-card>` (components/common/PodcastCard.vue)
- `<CommonPostCard>` | `<common-post-card>` (components/common/PostCard.vue)
- `<LayoutAppFooter>` | `<layout-app-footer>` (components/layout/AppFooter.vue)
- `<LayoutAppHeader>` | `<layout-app-header>` (components/layout/AppHeader.vue)
- `<LayoutContactsBlock>` | `<layout-contacts-block>` (components/layout/ContactsBlock.vue)
- `<LayoutMainMenu>` | `<layout-main-menu>` (components/layout/MainMenu.vue)
- `<LayoutProgressBar>` | `<layout-progress-bar>` (components/layout/ProgressBar.vue)
- `<UiAppBtn>` | `<ui-app-btn>` (components/ui/AppBtn.vue)
- `<UiAppForm>` | `<ui-app-form>` (components/ui/AppForm.vue)
