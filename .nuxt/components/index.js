import { wrapFunctional } from './utils'

export { default as CommonPersonCard } from '../../components/common/PersonCard.vue'
export { default as CommonPodcastCard } from '../../components/common/PodcastCard.vue'
export { default as CommonPostCard } from '../../components/common/PostCard.vue'
export { default as LayoutAppFooter } from '../../components/layout/AppFooter.vue'
export { default as LayoutAppHeader } from '../../components/layout/AppHeader.vue'
export { default as LayoutContactsBlock } from '../../components/layout/ContactsBlock.vue'
export { default as LayoutMainMenu } from '../../components/layout/MainMenu.vue'
export { default as LayoutProgressBar } from '../../components/layout/ProgressBar.vue'
export { default as UiAppBtn } from '../../components/ui/AppBtn.vue'
export { default as UiAppForm } from '../../components/ui/AppForm.vue'

export const LazyCommonPersonCard = import('../../components/common/PersonCard.vue' /* webpackChunkName: "components/common-person-card" */).then(c => wrapFunctional(c.default || c))
export const LazyCommonPodcastCard = import('../../components/common/PodcastCard.vue' /* webpackChunkName: "components/common-podcast-card" */).then(c => wrapFunctional(c.default || c))
export const LazyCommonPostCard = import('../../components/common/PostCard.vue' /* webpackChunkName: "components/common-post-card" */).then(c => wrapFunctional(c.default || c))
export const LazyLayoutAppFooter = import('../../components/layout/AppFooter.vue' /* webpackChunkName: "components/layout-app-footer" */).then(c => wrapFunctional(c.default || c))
export const LazyLayoutAppHeader = import('../../components/layout/AppHeader.vue' /* webpackChunkName: "components/layout-app-header" */).then(c => wrapFunctional(c.default || c))
export const LazyLayoutContactsBlock = import('../../components/layout/ContactsBlock.vue' /* webpackChunkName: "components/layout-contacts-block" */).then(c => wrapFunctional(c.default || c))
export const LazyLayoutMainMenu = import('../../components/layout/MainMenu.vue' /* webpackChunkName: "components/layout-main-menu" */).then(c => wrapFunctional(c.default || c))
export const LazyLayoutProgressBar = import('../../components/layout/ProgressBar.vue' /* webpackChunkName: "components/layout-progress-bar" */).then(c => wrapFunctional(c.default || c))
export const LazyUiAppBtn = import('../../components/ui/AppBtn.vue' /* webpackChunkName: "components/ui-app-btn" */).then(c => wrapFunctional(c.default || c))
export const LazyUiAppForm = import('../../components/ui/AppForm.vue' /* webpackChunkName: "components/ui-app-form" */).then(c => wrapFunctional(c.default || c))
