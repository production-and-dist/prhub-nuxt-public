export default {
    loadingIndicator: {
      name: 'nuxt',
      color: '#EA4D51',
      background: '#1b1d1e'
    },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title:'PRHub',
    titleTemplate: '%s | PRHub',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'О проекте pr hub — это серия интервью с экспертами в области pr, рекламы, маркетинга и коммуникаций' },

        {name:'msapplication-TileColor',content:'#333333'},
        {name:'theme-color',content:'#ffffff'},

        { property: 'og:title', content:'PRHub'},
        { property: 'og:description', content:'О проекте pr hub — это серия интервью с экспертами в области pr, рекламы, маркетинга и коммуникаций'},
        { property: 'og:image', content:'https://prhub.bw.com.kz/thumbnail.jpg'},
        { property: 'og:url', content:'https://prhub.bw.com.kz'},
        { name: 'twitter:card', content:'summary_large_image'},

        {name: 'twitter:image:alt', content:'twitter:image:alt' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', href: '/apple-touch-icon.png', sizes:'180x180' },
      { rel: 'icon', type: 'image/png', href: '/favicon-32x32.png', sizes:'32x32' },
      { rel: 'icon', type: 'image/png', href: '/favicon-16x16.png', sizes:'16x16' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg' },
    ],
    script:[
        {
            src: "https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js",
        },
        {
            src: "https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js",
        },
        {
            src: "https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js",
        },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
        '~/assets/scss/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/google-analytics'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/yandex-metrika',
  ],

  yandexMetrika: {
    id: '77526913',
    webvisor: true,
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  googleAnalytics: {
    id: 'G-0EKCP817H5'
  }
}
